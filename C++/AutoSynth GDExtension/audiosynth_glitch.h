#ifndef AUDIOSYNTH_GLITCH_H
#define AUDIOSYNTH_GLITCH_H

#include "audiosynth.h"

namespace godot {
  
  class AudioSynthGlitch : public AudioSynth {
    GDCLASS(AudioSynthGlitch, AudioSynth)
    
    private:
      int change_frequency;

      int fx_bitcrush;

      virtual float _generate_audio(int frequency, int sample_rate, int sample_count) override;

    protected:
	    static void _bind_methods();

      void set_change_frequency(const int change_frequency);
	    int get_change_frequency() const;

    public:
      AudioSynthGlitch();
  };

}

#endif
