#include <godot_cpp/classes/engine.hpp>
#include <godot_cpp/core/math.hpp>
#include <godot_cpp/classes/audio_stream_generator_playback.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/classes/property_tweener.hpp>
#include <godot_cpp/classes/tween.hpp>
#include <godot_cpp/classes/global_constants.hpp>

#include "audiosynth.h"

using namespace godot;

void AudioSynth::_bind_methods() {
    ClassDB::bind_method(D_METHOD("get_sample_rate"), &AudioSynth::get_sample_rate);
    ClassDB::bind_method(D_METHOD("set_sample_rate", "sample_rate"), &AudioSynth::set_sample_rate);
    ClassDB::add_property("AudioSynth", PropertyInfo(Variant::INT, "sample_rate"), "set_sample_rate", "get_sample_rate");

    ClassDB::bind_method(D_METHOD("get_frequency"), &AudioSynth::get_frequency);
    ClassDB::bind_method(D_METHOD("set_frequency", "frequency"), &AudioSynth::set_frequency);
    ClassDB::add_property("AudioSynth", PropertyInfo(Variant::INT, "frequency"), "set_frequency", "get_frequency");

    ClassDB::bind_method(D_METHOD("get_volume"), &AudioSynth::get_volume);
    ClassDB::bind_method(D_METHOD("set_volume", "volume"), &AudioSynth::set_volume);
    ClassDB::add_property("AudioSynth", PropertyInfo(Variant::FLOAT, "volume"), "set_volume", "get_volume");

    ClassDB::bind_method(D_METHOD("get_player"), &AudioSynth::get_player);
    ClassDB::bind_method(D_METHOD("set_player", "player"), &AudioSynth::set_player);
    ClassDB::add_property("AudioSynth", PropertyInfo(Variant::NODE_PATH, "player_path"), "set_player", "get_player"); 

    ClassDB::bind_method(D_METHOD("tween_frequency", "frequency", "time"), &AudioSynth::tween_frequency);
    ClassDB::bind_method(D_METHOD("tween_volume", "volume", "time"), &AudioSynth::tween_volume);
    ClassDB::bind_method(D_METHOD("tween_volume_envelope", "curve", "length"), &AudioSynth::tween_volume_envelope);
    ClassDB::bind_method(D_METHOD("cancel_tweens"), &AudioSynth::cancel_tweens);
}

AudioSynth::AudioSynth() {
    // Constructor
    set_process(true);

    sample_rate = 44100;
    frequency = 440.0f;
    volume = 1.0f;

    sample_count = 0;

    player = nullptr;

    envelope_active = false;
}

AudioSynth::~AudioSynth() {
    // Destructor
}

void AudioSynth::_notification(int p_what) {
    if (Engine::get_singleton()->is_editor_hint()) {
        // Only run in-game
        return;
    }


    if (p_what == NOTIFICATION_READY) {
        player = get_node<AudioStreamPlayer3D>(player_path);
    } else if (p_what == NOTIFICATION_PROCESS) {

    // Only fill buffer if sound is playing
    if (player != nullptr && player->is_playing()){
        _fill_buffer();
    }
  }
}

/**
 * Fills the audio buffer
*/
void AudioSynth::_fill_buffer() {
    if (!player->has_stream_playback()){
        UtilityFunctions::print("Error in audiosynth: stream playback not found!");
        return;
    }

    Ref<AudioStreamGeneratorPlayback> playback = (Ref<AudioStreamGeneratorPlayback>)(player->get_stream_playback());

    int to_fill = playback->get_frames_available();
    while (to_fill > 0) {
        float wave = 0;

        // Only run if audible
        if (volume > 0) { 
            //Generate wave using unique function - o
            wave = _generate_audio(frequency, sample_rate, sample_count);

            // Multiply by .5 to prevent distortion, then by volume
            wave = wave * .5 * volume;
        }

        // Push sample
        playback->push_frame(Vector2(1.0f,1.0f) * (wave * volume));
        to_fill--;
        sample_count++;

        // Advance tweens
        if (frequency_tween != nullptr && frequency_tween->is_valid()) {
            frequency_tween->custom_step(1.0f/sample_rate);
        }
        if (volume_tween != nullptr && volume_tween->is_valid()) {
                volume_tween->custom_step(1.0f/sample_rate);
        }

        // Envelope
        if (envelope_active) {
            float time = ((sample_count - envelope_start) / sample_rate) / envelope_length;

            if (time >= envelope_length) {
            envelope_active = false;
            } else {
            volume = envelope_curve->sample(time);
            }

        }
    }
}

void AudioSynth::tween_frequency( const int frequency, const float time ) {
    if (frequency_tween != nullptr) {
        frequency_tween->kill();
    }

    frequency_tween = create_tween();
    frequency_tween->tween_property(this, NodePath("frequency"), frequency, time);
    frequency_tween->pause();
}

void AudioSynth::tween_volume( const float volume, const float time ) {
    if (volume_tween != nullptr) {
        volume_tween->kill();
    }

    volume_tween = create_tween();
    volume_tween->tween_property(this, NodePath("volume"), volume, time);
    volume_tween->pause();

    // In case volume is 0
    volume_tween->custom_step(1.0f/sample_rate);
}

void AudioSynth::tween_volume_envelope(const Ref<Curve> & curve, float length) {
    // if (volume_tween != nullptr) {
    //   volume_tween->kill();
    // }
    envelope_active = true;
    envelope_curve = curve;
    envelope_length = length;
    envelope_start = sample_count;
}

void AudioSynth::cancel_tweens() {
    if (volume_tween != nullptr) {
        volume_tween->kill();
    }
    if (frequency_tween != nullptr) {
        volume_tween->kill();
    }
    envelope_active = false;
}

float AudioSynth::_generate_audio(int frequency, int sample_rate, int sample_count) {
    return 0;
}

void AudioSynth::set_sample_rate(const int sample_rate) {
    this->sample_rate = sample_rate;
}

int AudioSynth::get_sample_rate() const {
    return sample_rate;
}

void AudioSynth::set_frequency(const int frequency) {
    this->frequency = frequency;
}

int AudioSynth::get_frequency() const {
    return frequency;
}

void AudioSynth::set_volume(const float volume) {
    this->volume = volume;
}

float AudioSynth::get_volume() const {
    return volume;
}

void AudioSynth::set_player(const NodePath player_path) {
    this->player_path = player_path;
}

NodePath AudioSynth::get_player() const {
    return player_path;
}