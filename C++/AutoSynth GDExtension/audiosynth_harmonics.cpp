#include <godot_cpp/variant/utility_functions.hpp>

#include "audiosynth_harmonics.h"

using namespace godot;

AudioSynthHarmonics::AudioSynthHarmonics() {
    step = 2;
    count = 10;
    reduction = 0.5f;
}

void AudioSynthHarmonics::_bind_methods() {
    ClassDB::bind_method(D_METHOD("get_step"), &AudioSynthHarmonics::get_step);
    ClassDB::bind_method(D_METHOD("set_step", "harmonics"), &AudioSynthHarmonics::set_step);
    ClassDB::add_property("AudioSynthHarmonics", PropertyInfo(Variant::INT, "step"), "set_step", "get_step");

    ClassDB::bind_method(D_METHOD("get_count"), &AudioSynthHarmonics::get_count);
    ClassDB::bind_method(D_METHOD("set_count", "harmonics"), &AudioSynthHarmonics::set_count);
    ClassDB::add_property("AudioSynthHarmonics", PropertyInfo(Variant::INT, "count"), "set_count", "get_count");

    ClassDB::bind_method(D_METHOD("get_reduction"), &AudioSynthHarmonics::get_reduction);
    ClassDB::bind_method(D_METHOD("set_reduction", "reduction"), &AudioSynthHarmonics::set_reduction);
    ClassDB::add_property("AudioSynthHarmonics", PropertyInfo(Variant::FLOAT, "reduction", PROPERTY_HINT_RANGE, "0, 1"), "set_reduction", "get_reduction");
}

/**
 * Fills the audio buffer
*/
float AudioSynthHarmonics::_generate_audio(int frequency, int sample_rate, int sample_count) {
    double wave = 0.0f;

    // square wave
    for (int i = 1; i < count; i += step) {
        wave += sin(frequency * i * (double)sample_count/(double)sample_rate * TAU) * pow(reduction, i - 1);
    }

    // Push sample
    return wave;
}

void AudioSynthHarmonics::set_step(const int step) {
    this->step = step;
}

int AudioSynthHarmonics::get_step() const {
    return step;
}

void AudioSynthHarmonics::set_count(const int count) {
    this->count = count;
}

int AudioSynthHarmonics::get_count() const {
    return count;
}

void AudioSynthHarmonics::set_reduction(const float reduction) {
    this->reduction = reduction;
}

float AudioSynthHarmonics::get_reduction() const {
    return reduction;
}