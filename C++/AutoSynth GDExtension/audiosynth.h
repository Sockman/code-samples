#ifndef AUDIOSYNTH_H
#define AUDIOSYNTH_H

#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/classes/curve.hpp>
#include <godot_cpp/classes/audio_stream_player3d.hpp>

#define TAU 6.283185307179586

namespace godot {
  
    class AudioSynth : public Node {
        GDCLASS(AudioSynth, Node)

        private:
            // Export vars
            int sample_rate;
            int frequency;
            float volume;
            NodePath player_path;

            // Private vars
            AudioStreamPlayer3D* player;
            int sample_count;

            // Tweens
            Ref<Tween> frequency_tween;
            Ref<Tween> volume_tween;

            // For envelope
            bool envelope_active;
            Ref<Curve> envelope_curve;
            float envelope_length;
            float envelope_start;


            // Fills the audio buffer using _generate_audio()
            void _fill_buffer();

            // Override this function to write code that generates individual samples
            virtual float _generate_audio(int frequency, int sample_rate, int sample_count);

        protected:
            static void _bind_methods();

            void _notification(int p_what);

        public:
            AudioSynth();
            ~AudioSynth();
            
            // void _ready() override;
            // void _process(double delta) override;

            void tween_frequency( const int frequency, const float time );
            void tween_volume( const float volume, const float time );
            void tween_volume_envelope( const Ref<Curve> & curve, float length );
            void cancel_tweens();

            void set_sample_rate(const int amplitude);
            int get_sample_rate() const;

            void set_frequency(const int frequency);
            int get_frequency() const;

            void set_volume(const float volume);
            float get_volume() const;

            void set_player(const NodePath player_path);
            NodePath get_player() const;
    };
}

#endif
