This was a GDExtension created for Void Tapes: https://sockman.itch.io/void-tapes

It allows for unique real-synthesizers to be added in-game, and allows developers to use them in other Godot scripts.

The base class is AudioSynth, and the derived classes AudioSynthGlitch and AudioSynthHarmonics implement their own unique synthesizers.



I originally wrote this in GDScript, but it was too slow to manage a 44.1k sample rate. This was able to manage that on almost any hardware. 