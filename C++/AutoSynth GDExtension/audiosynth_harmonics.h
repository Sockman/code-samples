#ifndef AUDIOSYNTH_SQUARE_H
#define AUDIOSYNTH_SQUARE_H

#include "audiosynth.h"

namespace godot {
  
    class AudioSynthHarmonics : public AudioSynth {
        GDCLASS(AudioSynthHarmonics, AudioSynth)
        
        private:
            int step;
            int count;
            float reduction;

            virtual float _generate_audio(int frequency, int sample_rate, int sample_count) override;

        protected:
            static void _bind_methods();

            void set_step(const int step);
                int get_step() const;

            void set_count(const int count);
                int get_count() const;

            void set_reduction(const float count);
                float get_reduction() const;

        public:
            AudioSynthHarmonics();
    };
}

#endif
