#include <godot_cpp/variant/utility_functions.hpp>

#include "audiosynth_glitch.h"

using namespace godot;

AudioSynthGlitch::AudioSynthGlitch() {
    change_frequency = 1000;
}

void AudioSynthGlitch::_bind_methods() {
    ClassDB::bind_method(D_METHOD("get_change_frequency"), &AudioSynthGlitch::get_change_frequency);
    ClassDB::bind_method(D_METHOD("set_change_frequency", "change_frequency"), &AudioSynthGlitch::set_change_frequency);
    ClassDB::add_property("AudioSynthGlitch", PropertyInfo(Variant::INT, "change_frequency"), "set_change_frequency", "get_change_frequency");
}

/**
 * Fills the audio buffer
*/
float AudioSynthGlitch::_generate_audio(int frequency, int sample_rate, int sample_count) {
    double wave = 0.0f;

    // square wave
    for (int i = 1; i < 50; i += 3) {
        wave += sin(frequency * i * ((double)sample_count/(double)sample_rate) * TAU) * (1.0/i);
    }

    // modulating bitcrush
    wave = round(0.5 * (wave + 1) * fx_bitcrush) / float(fx_bitcrush) - 1.0f;
    // change bitcrush amount every change_frequency samples
    if (sample_count % change_frequency == 0) {
        fx_bitcrush = UtilityFunctions::randi_range(1, 20);
    }

    // Push sample
    return wave;
}

void AudioSynthGlitch::set_change_frequency(const int change_frequency) {
    this->change_frequency = change_frequency;
}

int AudioSynthGlitch::get_change_frequency() const {
    return change_frequency;
}
