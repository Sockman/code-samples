// These are the primary and secondary fire function for the gravity gun in Half-Life 2 Classic,
// a mod of Half-Life 1 that recreates Half-Life 2.

// "func_pushables" are the main physics entity. Primary attack will apply a force.
// Secondary attack will hold the object in front of the player.

// Half-Life 1 has a much worse physics engine than Half-Life 2, so this was a fun challenge.
// There are some special cases for specific entities, such as headcrabs, which take a small
// amount of damage.

void CGGUN::PrimaryAttack()
{
  usingSecondary = false;


  int flags;
#if defined( CLIENT_WEAPONS )
  flags = FEV_NOTHOST;
#else
  flags = 0;
#endif
  m_pPlayer->SetAnimation(PLAYER_ATTACK1);
#ifndef CLIENT_DLL
  TraceResult tr;

  UTIL_MakeVectors( m_pPlayer->pev->v_angle );
  Vector vecStart = m_pPlayer->GetGunPosition( );
  Vector vecEnd = vecStart + gpGlobals->v_forward * GGUN_PUSH_RANGE;
  if (holdState == holding){
          UTIL_TraceHull(vecStart, vecEnd, dont_ignore_monsters, head_hull, ENT(m_pPlayer->pev), &tr);
  }
  else{
    UTIL_TraceLine(vecStart, vecEnd, dont_ignore_monsters, ENT(m_pPlayer->pev), &tr);	//ENT(pev)
  }

  bool shouldDmg = false;
  if (tr.pHit)
  {

    m_flNextPrimaryAttack = GetNextAttackDelay(0.75);

    CBaseEntity *pEntityb = CBaseEntity::Instance(tr.pHit);
    if(FClassnameIs(pEntityb->pev, "func_breakable")){
      shouldDmg = true;
      //pEntityb->TakeDamage(pev, pev, gSkillData.plrDmgGgun, DMG_CLUB);
    }
    else if(FClassnameIs(pEntityb->pev, "func_pushable")){
            pEntityb->pev->velocity = gpGlobals->v_forward * GGUN_PUSH_FORCE;
    }
    else if (FClassnameIs(pEntityb->pev, "monster_headcrab") || FClassnameIs(pEntityb->pev, "monster_fast_headcrab") || FClassnameIs(pEntityb->pev, "monster_poison_headcrab")){
            pEntityb->pev->velocity = gpGlobals->v_forward * GGUN_PUSH_FORCE/2;
            shouldDmg = true;
            //pEntityb->TakeDamage(pev, pev, gSkillData.plrDmgGgun, DMG_CLUB);
    }
  }
  else{
    m_flNextPrimaryAttack = GetNextAttackDelay(1);
  }
  if (shouldDmg){
    //Do normal gun stuff here so it damages stuff
    // player "shoot" animation
    m_pPlayer->SetAnimation(PLAYER_ATTACK1);

    Vector vecSrc = m_pPlayer->GetGunPosition();
    Vector vecAiming = m_pPlayer->GetAutoaimVector(AUTOAIM_5DEGREES);
    Vector vecDir;

#ifdef CLIENT_DLL
    if (!bIsMultiplayer())
#else
    if (!g_pGameRules->IsMultiplayer())
#endif
    {
      // optimized multiplayer. Widened to make it easier to hit a moving player
      vecDir = m_pPlayer->FireBulletsPlayer(1, vecSrc, vecAiming, VECTOR_CONE_6DEGREES, 8192, BULLET_NONE, 2, 0, m_pPlayer->pev, m_pPlayer->random_seed);
    }
    else
    {
      // single player spread
      vecDir = m_pPlayer->FireBulletsPlayer(1, vecSrc, vecAiming, VECTOR_CONE_3DEGREES, 8192, BULLET_NONE, 2, 0, m_pPlayer->pev, m_pPlayer->random_seed);
    }
    //End of normal gun stuff
  }
  holdState = none;
#endif

  PLAYBACK_EVENT_FULL(flags, m_pPlayer->edict(), m_usGGUNFire, 0.0, (float *)&m_pPlayer->pev->origin, (float *)&m_pPlayer->pev->angles, 0.0, 0.0, 0, 0, 0, 1);

  if (m_flNextPrimaryAttack < UTIL_WeaponTimeBase())
	  m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 0.1;
  m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + UTIL_SharedRandomFloat( m_pPlayer->random_seed, 10, 15 );
}


void CGGUN::SecondaryAttack(void)
{
  int flags;
  #if defined( CLIENT_WEAPONS )
  flags = FEV_NOTHOST;
  #else
  flags = 0;
  #endif
  
  if (usingSecondary){	//updates every frame
    Vector fakeOrigin = (pEntity->pev->absmin + pEntity->pev->absmax) / 2;

    float biggerSize;
    if (pEntity->pev->size.x > pEntity->pev->size.y){
      biggerSize = pEntity->pev->size.x;
    }
    else{
      biggerSize = pEntity->pev->size.y;
    }

    Vector forwardVel;
    
    forwardVel = gpGlobals->v_forward * biggerSize * 2;
    if (biggerSize <= 16){
	    forwardVel = gpGlobals->v_forward * biggerSize * 5;
    }

    switch (holdState){
      case none:
        usingSecondary = false;
        return;
        break;
      case dragging:
        //pEntity->pev->velocity = gpGlobals->v_forward * -(GGUN_PULL_FORCE / Distance(fakeOrigin, m_pPlayer->GetGunPosition() - forwardVel));
        pEntity->pev->velocity = ((m_pPlayer->GetGunPosition() + forwardVel) - fakeOrigin) * (GGUN_PULL_FORCE / Distance(fakeOrigin, m_pPlayer->GetGunPosition() - forwardVel));
        break;
      case holding:
        pEntity->pev->velocity = ((m_pPlayer->GetGunPosition() + forwardVel) - fakeOrigin) * GGUN_PULL_MAGNETISM;
        if (abs(pEntity->pev->velocity.x) + abs(pEntity->pev->velocity.y) + abs(pEntity->pev->velocity.z) > 650){	//limit velocity so stuff doesn't explode
          pEntity->pev->velocity = Vector(pEntity->pev->velocity.x / 2, pEntity->pev->velocity.y / 2, pEntity->pev->velocity.z / 2);
        }
        break;
      default:
        break;
    }
    if (holdState != dropped){
      //Rotate pushable according to angles
      pEntity->pev->angles = Vector(pEntity->pev->angles.x / 2, pEntity->pev->angles.y / 2, pEntity->pev->angles.z / 2);//Vector(0, 0, 0);

    if (Distance(m_pPlayer->GetGunPosition(), fakeOrigin) > GGUN_PULL_RANGE){
      holdState = none;
    }
    else if (Distance(m_pPlayer->GetGunPosition(), fakeOrigin) > Distance(fakeOrigin, forwardVel)){		//biggerSize * 3
      holdState = dragging;
    }
    else if(holdState != holding){
      holdState = holding;
      SendWeaponAnim(GGUN_PICKUP);
    }


    }
    if (acos(DotProduct(m_pPlayer->GetGunPosition() - fakeOrigin, gpGlobals->v_forward) /
        (Distance(Vector(0, 0, 0), m_pPlayer->GetGunPosition() - fakeOrigin) *
          Distance(Vector(0, 0, 0), gpGlobals->v_forward))) < 1.5) {
      m_flNextPrimaryAttack = m_flNextSecondaryAttack = GetNextAttackDelay(1);
      holdState = none;
    }
    return;
  }
  #ifndef CLIENT_DLL

  //New click (happens once at the start of a click)
  //If i'm already holding something, drop it.
  if(holdState == holding){
    holdState = dropped;
    usingSecondary = true;
  }
  else{
    //Get object player is looking at
    UTIL_MakeVectors(m_pPlayer->pev->v_angle);
    vecStart = m_pPlayer->GetGunPosition();
    vecEnd = vecStart + gpGlobals->v_forward * GGUN_PULL_RANGE;
    UTIL_TraceLine(vecStart, vecEnd, ignore_monsters, ENT(m_pPlayer->pev), &tr);
    pEntity = CBaseEntity::Instance(tr.pHit);
    if(FClassnameIs(pEntity->pev, "func_pushable")){
      holdState = dragging;
      usingSecondary = true;
      FireTargets(STRING(pEntity->pev->message), eoNullEntity, this, USE_TOGGLE, 0);
    }
    else{
      holdState = none;
    }
  }

  if (holdState == dropped){
    SendWeaponAnim(GGUN_DROP);
  }
  else{
    SendWeaponAnim(GGUN_IDLE1);
  }
  #endif
  m_flNextPrimaryAttack = m_flNextSecondaryAttack = GetNextAttackDelay(.01);
}

