# This is GDScript code for my game Year Unknown.
# (GDScript is the coding language for the Godot game engine)
# This code manages character portraits in the game.
# To see more information about this code, watch this video: https://youtu.be/HOsSjW_SPyE

extends Control


@export var blink_probablility_curve : Curve

var a_emotions : Array[Curve2D]
var b_emotions : Array[Curve2D]

var current_curve : Curve2D
var next_curve : Curve2D
var is_visible := false
var current_emotion := DialogueManager.EMOTIONS.neutral
var current_character : DialogueManager.CHARACTERS

var time_since_last_idle := 0.0
var time_until_next_idle := 5.0

var time_since_blink := 0.0
var time_until_next_blink := 1.0

var lerp_timer := 0.0

func _ready() -> void:
	a_emotions = []
	b_emotions = []
	for child in %A_Expressions.get_children():
		a_emotions.append(child.curve)
	for child in %B_Expressions.get_children():
		b_emotions.append(child.curve)
	
	current_curve = a_emotions[0].duplicate()
	next_curve = current_curve

func _process(delta : float) -> void:
	%Eye.visible = is_visible;
	%Eye3.visible = is_visible;
	if !is_visible:
		next_curve = a_emotions[DialogueManager.EMOTIONS.neutral]
		for i in range(current_curve.point_count):
			current_curve.set_point_in(i, next_curve.get_point_in(i))
			current_curve.set_point_out(i, next_curve.get_point_out(i))
			current_curve.set_point_position(i, next_curve.get_point_position(i))
		return
		
	# Visibility
	if current_character == DialogueManager.CHARACTERS.a:
		%Eye.self_modulate.a = 1
		%Eye2.self_modulate.a = 0
		%Eye3.self_modulate.a = 0
	elif current_character == DialogueManager.CHARACTERS.b:
		%Eye.self_modulate.a = 0
		%Eye2.self_modulate.a = 1
		%Eye3.self_modulate.a = 0
	else:
		%Eye.self_modulate.a = 0
		%Eye2.self_modulate.a = 0
		%Eye3.self_modulate.a = 1
		
	if next_curve == null:
		return
		
	# Update line renderer
	%Eye.set_polygon(current_curve.get_baked_points())
	%Eye2.set_points(current_curve.get_baked_points())
	
	# update eye shape
	for i in range(current_curve.point_count):
		current_curve.set_point_in(i, lerp(current_curve.get_point_in(i), next_curve.get_point_in(i), lerp_timer))
		current_curve.set_point_out(i, lerp(current_curve.get_point_out(i), next_curve.get_point_out(i), lerp_timer))
		current_curve.set_point_position(i, lerp(current_curve.get_point_position(i), next_curve.get_point_position(i), lerp_timer))
	
	# blinking
	if time_since_blink > time_until_next_blink:
		time_since_blink = 0
		time_until_next_blink = 0.4 + sqrt(randf()) * 15
		$Animations/Blink.play("blink")
	
	# idle animations
	if time_since_last_idle > time_until_next_idle:
		time_since_last_idle = 0
		var animation_player : AnimationPlayer = null
		
		match current_emotion:
			DialogueManager.EMOTIONS.neutral:
				animation_player = $Animations/Neutral
		
		
		if animation_player:
			animation_player.play(animation_player.get_animation_list()[randi_range(1, animation_player.get_animation_list().size() - 1)])
			time_until_next_idle = animation_player.current_animation_length + randf_range(2, 10)
	
	# update timers
	lerp_timer = min(lerp_timer + delta * 0.5, 1.0)
	time_since_last_idle += delta
	time_since_blink += delta
	

func update_emotion(emotion : DialogueManager.EMOTIONS, character : DialogueManager.CHARACTERS):
	match(character):
		DialogueManager.CHARACTERS.computer:
			current_character = character
			current_emotion = emotion
			lerp_timer = 0
			is_visible = true
			next_curve = null
		DialogueManager.CHARACTERS.a:
			current_character = character
			current_emotion = emotion
			lerp_timer = 0
			is_visible = true
			next_curve = a_emotions[emotion]
		DialogueManager.CHARACTERS.b:
			current_character = character
			current_emotion = emotion
			lerp_timer = 0
			is_visible = true
			next_curve = b_emotions[emotion]
		_:
			lerp_timer = 0
			is_visible = false

