# Code Samples

This has examples of code written by me for various projects.

Comments at the top of each file provide context.

Everything is sorted by programming language, and files are named in the following format:
`{ProjectName}_{CodeFunction}.{LanguageExtension}`