// This was from an unreleased Unity project where I experimented with interactive audio.
// This script was for controlling the playback of an authored sequence of samples ("notes")
// Here, I've combined the code for the Scriptable Object, and the implementation class using it.

[System.Serializable]
public struct note
{
    public AudioClip clip;
    public float pitch;
}

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/NoteSequence", order = 1)]
public class NoteSequence : ScriptableObject
{
    public note[] sequence;
}

// The implementation class:

public class NotePlayer
{
    private NoteSequence noteSequence;

    private int currentNote;

    public void SetNewSequence(NoteSequence newSequence)
    {
        currentNote = 0;
        noteSequence = newSequence;
    }

    public void ResetSequence()
    {
        currentNote = 0;
    }

    public void PlayNextNote(AudioSource source)
    {
        source.pitch = noteSequence.sequence[currentNote].pitch;
        source.PlayOneShot(noteSequence.sequence[currentNote].clip);
        currentNote++;
        if(currentNote >= noteSequence.sequence.Length)
        {
            currentNote = 0;
        }
    }

    // Helper function for playing a random note
    public static void PlayRandom(AudioSource source, NoteSequence sequence)
    {
        source.PlayOneShot(sequence.sequence[Random.Range(0, sequence.sequence.Length)].clip);
    }
}