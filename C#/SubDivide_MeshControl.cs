// This was for my game SubDivide, and handled the visuals of the division.
// It controls the vertices of two meshes which display render textures for the two "levels" the player can move between.
// It was last changed in 2020, so it has some oddities (such as FindGameObjectWithTag being called in update) that I wouldn't do if I wrote it today.


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MeshControl : MonoBehaviour
{
    public int whichPlane;


    public float screenWidth = 5.0f * (16.0f / 9.0f);
    public readonly float screenHeight = 5.0f;

    public float collisionExtentX = 30;
    private float collisionExtentY = 2;

    PolygonCollider2D col;
    Vector2[] pointsInit;

    private Transform hand;


    // Start is called before the first frame update
    void Start()
    {
        hand = GameObject.FindGameObjectWithTag("Hand").transform;

        col = GetComponent<PolygonCollider2D>();
        pointsInit = col.points;

        updateMeshAspect();
    }


    public GameObject previewObject;


    public MeshRenderer levelPreview1 = null;
    public MeshRenderer levelPreview2 = null;
    public MeshRenderer levelPreview3 = null;
    [SerializeField]
    Material[] rtMats = null;


    [SerializeField]
    PlayerMovement player = null;

    public void showMesh(InputAction.CallbackContext context)
    {
        if (player.enablePeek)    //Don't peek if the player can't control the hand
            return;


        if (context.canceled)
        {
            previewObject.SetActive(false);
            player.show();
        }
        else
        {
            previewObject.SetActive(true);
            player.hide();
        }
    }

    private void updatePreviewMeshes()
    {
        if (whichPlane == 1)
            return;
        Mesh mesh1 = levelPreview1.GetComponent<MeshFilter>().mesh;
        Mesh mesh2 = levelPreview2.GetComponent<MeshFilter>().mesh;
        Mesh mesh3 = levelPreview3.GetComponent<MeshFilter>().mesh;
        mesh1.Clear();
        mesh2.Clear();

        levelData ld = GameObject.FindGameObjectWithTag("LevelData").GetComponent<levelData>();

        int previewNum = ld.lastScreen - 1;

        Vector3[] newVerticies = new Vector3[] { new Vector3(-screenWidth, -screenHeight / 2, 0), new Vector3(0, -screenHeight / 2, 0), new Vector3(0, screenHeight / 2, 0), new Vector3(-screenWidth, screenHeight / 2, 0) };
        Vector3[] newVerticies2 = new Vector3[] { new Vector3(0, -screenHeight / 2, 0), new Vector3(screenWidth, -screenHeight / 2, 0), new Vector3(screenWidth, screenHeight / 2, 0), new Vector3(0, screenHeight / 2, 0) };

        if (previewNum == 3)
        {
            newVerticies = new Vector3[]{ new Vector3(-screenWidth, -screenHeight / 3, 0), new Vector3(-screenWidth / 3, -screenHeight / 3, 0), new Vector3(-screenWidth / 3, screenHeight / 3, 0), new Vector3(-screenWidth, screenHeight / 3, 0)};
            newVerticies2 = new Vector3[]{ new Vector3(-screenWidth / 3, -screenHeight / 3, 0), new Vector3(screenWidth / 3, -screenHeight / 3, 0), new Vector3(screenWidth / 3, screenHeight / 3, 0), new Vector3(-screenWidth / 3, screenHeight / 3, 0) };
        }

        Vector2[] newUV = { new Vector2(0, 0), new Vector2(1.0f, 0), new Vector2(1.0f, 1.0f), new Vector2(0, 1.0f)};

        int[] newTriangles = { 2, 1, 0, 0, 3, 2 };

        mesh1.vertices = newVerticies;
        mesh1.uv = newUV;
        mesh1.triangles = newTriangles;
        mesh1.RecalculateNormals();

        mesh2.vertices = newVerticies2;
        mesh2.uv = newUV;
        mesh2.triangles = newTriangles;
        mesh2.RecalculateNormals();

        if (previewNum > 2)
        {
            newVerticies = new Vector3[] { new Vector3(screenWidth / 3, -screenHeight / 3, 0), new Vector3(screenWidth, -screenHeight / 3, 0), new Vector3(screenWidth, screenHeight / 3, 0), new Vector3(screenWidth / 3, screenHeight / 3, 0) };
            mesh3.vertices = newVerticies;
            mesh3.uv = newUV;
            mesh3.triangles = newTriangles;
            mesh3.RecalculateNormals();
        }

    }

    public void setPreviewMat(int previewTexture, int mat)
    {
        if (whichPlane == 1)
            return;

        switch(previewTexture)
        {
            case 0:
                levelPreview1.material = rtMats[mat];
                break;
            case 1:
                levelPreview2.material = rtMats[mat];
                break;
            case 2:
                levelPreview3.material = rtMats[mat];
                break;
        }
    }

    public void updateMeshAspect()
    {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().ResetAspect();
        screenWidth = screenHeight * GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().aspect;
        updateMeshes(GameObject.FindGameObjectWithTag("LevelData").GetComponent<levelData>().levelType);
        updatePreviewMeshes();
    }
    

    public void updateMeshes(int levelType)
    {
        if(hand == null)
        {
            hand = GameObject.FindGameObjectWithTag("Hand").transform;
        }

        switch (levelType)
        {
            case 0:
                horizontalLinearUpdate();
                break;
            case 1:
                radialUpdate();
                break;
            case 2:
                verticalLinearUpdate();
                break;
            case 3:
                doubleLinearUpdate();
                break;

            default:
                break;
        }
        
    }

    private void horizontalLinearUpdate()
    {

        float handPos = hand.position.x;

        Mesh mesh1 = GetComponent<MeshFilter>().mesh;

        mesh1.Clear();

        Vector3[] newVerticies = { new Vector3(-screenWidth, -screenHeight, 0), new Vector3(screenWidth, -screenHeight, 0), new Vector3(screenWidth, screenHeight, 0), new Vector3(-screenWidth, screenHeight, 0), new Vector3(handPos,screenHeight,0), new Vector3(handPos, -screenHeight, 0) };
        Vector2[] newUV = { new Vector2(0, 0), new Vector2(1.0f, 0), new Vector2(1.0f, 1.0f), new Vector2(0, 1.0f), new Vector2((handPos+screenWidth)/(screenWidth*2),1), new Vector2((handPos + screenWidth) / (screenWidth*2),0)  };


        int[] newTriangles = { };
        if (whichPlane == 1)
        {
            if(handPos >= screenWidth)
            {
                newTriangles = new int[] { 2, 1, 0, 0, 3, 2 };
            }
            else
            {
                newTriangles = new int[] { 5, 4, 2, 2, 1, 5 };
            }
        }
        else
        {
            if (handPos < screenWidth)
            {
                newTriangles = new int[] { 0, 3, 4, 4, 5, 0};
            }
        }

        mesh1.vertices = newVerticies;
        mesh1.uv = newUV;
        mesh1.triangles = newTriangles;
        mesh1.RecalculateNormals();


        Vector2[] points = new Vector2[newTriangles.Length];
        for (int i = 0; i < newTriangles.Length; i++)
        {
            points[i] = new Vector2(newVerticies[newTriangles[i]].x, newVerticies[newTriangles[i]].y);
        }

        //Set collider and add buffer for colliders out of bounds
        Vector2[] tempCol = points;
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i].x > -screenWidth && points[i].x < screenWidth)
            {
                if (points[i].x < -screenWidth+.2f || points[i].x > screenWidth-.2f)
                {
                    tempCol[i] = new Vector2(tempCol[i].x * collisionExtentX, tempCol[i].y * collisionExtentY);
                }
                else
                {
                    tempCol[i] = new Vector2(tempCol[i].x, tempCol[i].y * collisionExtentY);
                }
            }
            else
            {
                tempCol[i] = new Vector2(tempCol[i].x * collisionExtentX, tempCol[i].y * collisionExtentY);
            }
        }

        if (col != null)
        {
            col.points = tempCol;
        }
    }


    private void verticalLinearUpdate()
    {

        float handPos = hand.position.y;

        Mesh mesh1 = GetComponent<MeshFilter>().mesh;

        mesh1.Clear();

        Vector3[] newVerticies = { new Vector3(-screenWidth, -screenHeight, 0), new Vector3(screenWidth, -screenHeight, 0), new Vector3(screenWidth, screenHeight, 0), new Vector3(-screenWidth, screenHeight, 0), new Vector3(-screenWidth, handPos, 0), new Vector3(screenWidth, handPos, 0) };
        Vector2[] newUV = { new Vector2(0, 0), new Vector2(1.0f, 0), new Vector2(1.0f, 1.0f), new Vector2(0, 1.0f), new Vector2(0, (handPos + screenHeight) / (screenHeight * 2)), new Vector2(1, (handPos + screenHeight) / (screenHeight * 2)) };


        int[] newTriangles = { };
        if (whichPlane == 1)
        {
            if (handPos >= screenHeight)
            {
                newTriangles = new int[] { 2, 1, 0, 0, 3, 2 };
            }
            else
            {
                newTriangles = new int[] { 5, 4, 3, 3, 2, 5 };
            }
        }
        else
        {
            if (handPos < screenHeight)
            {
                newTriangles = new int[] { 0, 4, 5, 5, 1, 0 };
            }
        }

        mesh1.vertices = newVerticies;
        mesh1.uv = newUV;
        mesh1.triangles = newTriangles;
        mesh1.RecalculateNormals();


        Vector2[] points = new Vector2[newTriangles.Length];
        for (int i = 0; i < newTriangles.Length; i++)
        {
            points[i] = new Vector2(newVerticies[newTriangles[i]].x, newVerticies[newTriangles[i]].y);
        }

        //Set collider and add buffer for colliders out of bounds
        Vector2[] tempCol = points;
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i].y > -5 && points[i].y < 5)
            {
                if (points[i].y < -4.9 || points[i].y > 4.9)
                {
                    tempCol[i] = new Vector2(tempCol[i].x * collisionExtentX, tempCol[i].y * 2);
                }
                else
                {
                    tempCol[i] = new Vector2(tempCol[i].x * collisionExtentX, tempCol[i].y);
                    //tempCol[i] = new Vector2(tempCol[i].x * 2f, tempCol[i].y);
                }
            }
            else
            {
                tempCol[i] = new Vector2(tempCol[i].x * collisionExtentX, tempCol[i].y * 2);
            }
            /*
            else
            {
                tempCol[i] *= 2f;
            }
            */
        }

        if (col != null)
        {
            col.points = tempCol;
        }
    }


    //The commented lines within the next function are a reminder of what once was
    //I spend hours trying to make this division mode work with scrolling but all my attempts were failures
    //Until one day, when after many hours of pondering, I stumbled onto a solution, and that's to literally just see what edge the hand points to
    //So yeah i'm leaving this commented code here just to show how much I struggled with this
    //If you're seeing this email me at julianheuser@outlook.com. I'm curious as to why you're looking at this and I want to apologize personally for my terrible code, because I never anticipated that anyone but me would ever look at it.

    private void radialUpdate()
    {

        float handRot = hand.eulerAngles.z;
        Vector3 handPos = hand.position;

        float[] handPoint = { Mathf.Cos(handRot * (Mathf.PI / 180)) * 20, Mathf.Sin(handRot * (Mathf.PI / 180)) * 20 };


        sbyte[] isEdgeHit = { 0, 0 };

        if (Mathf.Abs(handPoint[0]) > screenWidth)
        {
            float factor = screenWidth / Mathf.Abs(handPoint[0]);
            handPoint[0] *= factor;
            handPoint[1] *= factor;
            isEdgeHit[0] = -1;
        }
        if (Mathf.Abs(handPoint[1]) > screenHeight)
        {
            float factor = screenHeight / Mathf.Abs(handPoint[1]);
            handPoint[0] *= factor;
            handPoint[1] *= factor;
        }


        handPoint[0] += handPos.x;
        if (Mathf.Abs(handPoint[0]) < screenWidth)
        {
            float factor = screenWidth / Mathf.Abs(handPoint[0]);
            handPoint[0] *= factor;
            handPoint[1] *= factor;
            isEdgeHit[0] = 1;
        }
        if (Mathf.Abs(handPoint[1]) > screenHeight)
        {
            float factor = screenHeight / Mathf.Abs(handPoint[1]);
            handPoint[0] *= factor;
            handPoint[1] *= factor;
            isEdgeHit[1] = 1;
        }


        Mesh mesh1 = GetComponent<MeshFilter>().mesh;

        mesh1.Clear();

        Vector3[] newVerticies = { handPos, new Vector3(screenWidth, screenHeight, 0), new Vector3(-screenWidth, screenHeight, 0), new Vector3(screenWidth, -screenHeight), new Vector3(-screenWidth, -screenHeight), new Vector3(-screenWidth, 0), new Vector3(handPoint[0], handPoint[1], 0) };

  
        float UVhandX = ((handPoint[0]/2f)) / screenWidth + .5f; //must be -.1 on left

        Vector2[] newUV = { new Vector2((handPos.x + screenWidth) /(2 * screenWidth), .5f), new Vector2(1, 1), new Vector2(0, 1), new Vector2(1, 0), new Vector2(0, 0), new Vector2(0, .5f), new Vector2(UVhandX, handPoint[1] / (screenHeight * 2) + .5f) };

        Vector3[] newColVerticies = { Vector3.zero, new Vector3(screenWidth, screenHeight, 0), new Vector3(-screenWidth, screenHeight, 0), new Vector3(screenWidth, -screenHeight), new Vector3(-screenWidth, -screenHeight), new Vector3(-screenWidth, 0), new Vector3(handPoint[0] - handPos.x, handPoint[1], 0) };


        bool leftEdge = handPoint[0] <= -screenWidth + .1f;			// isEdgeHit[0] == 1 && isEdgeHit[1] == 0;
        bool rightEdge = handPoint[0] >= screenWidth - .1f;			// isEdgeHit[0] == -1 && isEdgeHit[1] == 0;
        bool topbottomEdge = Mathf.Abs(handPoint[1]) >= screenHeight -.1f;	// isEdgeHit[1] == 1;


        int[] newTriangles = { };
        if (whichPlane == 1)
        {
            if (leftEdge && handRot <= 180)
            {
                newTriangles = new int[] { 0, 6, 2, 0, 2, 1, 0, 1, 3, 0, 3, 4, 0, 4, 5 };
            }
            else if (handRot <= 180 && handRot >= 0 && topbottomEdge)
            {
                newTriangles = new int[] { 0, 6, 1, 0, 1, 3, 0, 3, 4, 0, 4, 5 };
            }
            else if (rightEdge)//handRot <= 30 || (handRot <= 360 && handRot > 330))
            {
                newTriangles = new int[] { 0, 6, 3, 0, 3, 4, 0, 4, 5 };
            }
            else if (topbottomEdge)//handRot <= 330 && handRot >= 210)
            {
                newTriangles = new int[] { 0, 6, 4, 0, 4, 5 };
            }
            else if (leftEdge)//handRot < 210)
            {
                newTriangles = new int[] { 0, 6, 5 };
            }
        }
        else
        {
            if (handRot == 180)
            {
                newTriangles = new int[] { 0, 0, 0 };
            }
            else if (leftEdge && handRot <= 180)//handRot > 150 && handRot <= 180)
            {
                newTriangles = new int[] { 0, 5, 6 };
            }
            else if (handRot <= 180 && handRot >= 0 && topbottomEdge)//handRot <= 150 && handRot >= 30)
            {
                newTriangles = new int[] { 0, 5, 2, 0, 2, 6 };
            }
            else if (rightEdge)//handRot <= 30 || (handRot <= 360 && handRot > 330))
            {
                newTriangles = new int[] { 0, 5, 2, 0, 2, 1, 0, 1, 6 };
            }
            else if (topbottomEdge)//handRot <= 330 && handRot > 210)
            {
                newTriangles = new int[] { 0, 5, 2, 0, 2, 1, 0, 1, 3, 0, 3, 6 };
            }
            else if (leftEdge)//handRot <= 210)
            {
                newTriangles = new int[] { 0, 5, 2, 0, 2, 1, 0, 1, 3, 0, 3, 4, 0, 4, 6 };
            }
        }

        mesh1.vertices = newVerticies;
        mesh1.uv = newUV;
        mesh1.triangles = newTriangles;
        mesh1.RecalculateNormals();

        Vector2[] points = new Vector2[newTriangles.Length];
        for (int i = 0; i < newTriangles.Length; i += 1)
        {
            points[i] = new Vector2(newColVerticies[newTriangles[i]].x, newColVerticies[newTriangles[i]].y);
        }

        Vector2[] tempCol = points;
        for (int i = 0; i < points.Length; i++)
        {
            tempCol[i] *= 2f;
        }
        if (col != null)
        {
            col.points = tempCol;
        }
    }



    private void doubleLinearUpdate()
    {

        float handPos = hand.position.x;
        float altHandPos = -hand.position.x + (levelData.lineOfSymmetry*2);

        Mesh mesh1 = GetComponent<MeshFilter>().mesh;

        mesh1.Clear();

        Vector3[] newVerticies = { new Vector3(-screenWidth, -screenHeight, 0), new Vector3(screenWidth, -screenHeight, 0), new Vector3(screenWidth, screenHeight, 0), new Vector3(-screenWidth, screenHeight, 0), new Vector3(handPos, screenHeight, 0), new Vector3(handPos, -screenHeight, 0), new Vector3(altHandPos, screenHeight, 0), new Vector3(altHandPos, -screenHeight, 0), new Vector3(-9, screenHeight + 5, -20) };
        Vector2[] newUV = { new Vector2(0, 0), new Vector2(1.0f, 0), new Vector2(1.0f, 1.0f), new Vector2(0, 1.0f), new Vector2((handPos + screenWidth) / (screenWidth * 2), 1), new Vector2((handPos + screenWidth) / (screenWidth * 2), 0), new Vector2((altHandPos + screenWidth) / (screenWidth * 2), 1), new Vector2((altHandPos + screenWidth) / (screenWidth * 2), 0), Vector2.zero };


        int[] newTriangles = { };
        if (whichPlane == 1)
        {
            if (handPos >= screenWidth)
            {
                newTriangles = new int[] { 2, 1, 0, 0, 3, 2 };
            }
            else
            {
                newTriangles = new int[] { 6, 7, 5, 5, 4, 6 };
            }
        }
        else
        {
            if (handPos < screenWidth)
            {
                newTriangles = new int[] { 2, 1, 7, 7, 6, 2, 0, 3, 4, 4, 5, 0 };
            }
        }

        mesh1.vertices = newVerticies;
        mesh1.uv = newUV;
        mesh1.triangles = newTriangles;
        mesh1.RecalculateNormals();


        Vector2[] points = new Vector2[newTriangles.Length];
        for (int i = 0; i < newTriangles.Length; i++)
        {
            points[i] = new Vector2(newVerticies[newTriangles[i]].x, newVerticies[newTriangles[i]].y);
        }

        //Set collider and add buffer for colliders out of bounds
        Vector2[] tempCol = points;
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i].x > -screenWidth && points[i].x < screenWidth)
            {
                if (points[i].x < -screenWidth + .2f || points[i].x > screenWidth - .2f)
                {
                    tempCol[i] = new Vector2(tempCol[i].x * collisionExtentX, tempCol[i].y * collisionExtentY);
                }
                else
                {
                    tempCol[i] = new Vector2(tempCol[i].x, tempCol[i].y * collisionExtentY);
                }
            }
            else
            {
                tempCol[i] = new Vector2(tempCol[i].x * collisionExtentX, tempCol[i].y * collisionExtentY);
            }
        }

        if (col != null)
        {
            col.points = tempCol;
        }
    }
}
