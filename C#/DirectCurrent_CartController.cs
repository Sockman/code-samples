// This was for Direct Current, a game jam game (https://sockman.itch.io/direct-current)
// This is the "train" gameplay, where you switch between three lanes.
    // We didn't know that at the time, so this supports an arbitary number you can set from the editor
// It uses a lerp to smoothly move between each track
    // ...at the time I didn't know about framerate independent lerp (https://www.youtube.com/watch?v=LSNQuFEDOyQ)

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace TrainGame.TrainCart
{
    public class CartController : MonoBehaviour
    {
        [SerializeField]
        private Transform[] trackPositions;

        [SerializeField]
        private UnityEvent onCouple;
        [SerializeField]
        private AudioSource movementAudio;
        [SerializeField]
        private AudioSource coupleAudio;
        [SerializeField]
        private float standardMovementSpeed = 15.0f;

        private int _currentTrack = 0;

        private bool _readyForInput = true;

        void Update()
        {
            UpdatePitch();

            // Track changes

            float inputValue = InputManager.instance.move.y;

            if (inputValue == 0)
            {
                _readyForInput = true;
            }

            if (_readyForInput)
            {
                if (inputValue < 0)
                {
                    _currentTrack++;
                    _readyForInput = false;
                }
                else if (inputValue > 0)
                {
                    _currentTrack--;
                    _readyForInput = false;
                }
            }

            _currentTrack = Mathf.Clamp(_currentTrack, 0, trackPositions.Length - 1);

        }

        private void FixedUpdate()
        {
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, trackPositions[_currentTrack].position.y, .5f));
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Obstacle"))
            {
                collision.enabled = false;
                CartGameManager.instance.TakeDamage();
            }
            else if (collision.CompareTag("TrainCar"))
            {
                RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(.81f, 0), Vector2.right);

                if (hit.collider == null){
                    // We hit the car. Crash :(
                    collision.gameObject.GetComponent<TrainCar>().CrashCar();
                    CartGameManager.instance.TakeDamage();
                }
                else if (hit.collider.CompareTag("TrainCar"))
                {
                    // We hit the back of the car :)
                    collision.gameObject.GetComponent<TrainCar>().CoupleCar();
                    onCouple.Invoke();
                    coupleAudio.Play();
                }
            }
        }
    }
}
