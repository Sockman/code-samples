# This code was written for the Concepts of Computer Systems.
# It's written in MIPS assembly can decode or encode a message
# 	using the vigenere cipher.


.globl encrypt
.globl decrypt
.globl search_alphabet


# Name:		search_alphabet
#
# Description:	searches an alphabet for a character,
#		and returns it's index
#
# Arguments:	$a0 the size of alphabet 
# 		$a1 the address to the alphabet
# 		$a2 the character to search for
#
# Returns:	$v0 is index in alphabet of the character
# 		$v1 is 1 if the character isn't in alphabet
# 
# Destroys:	t0, t5, 

search_alphabet:
	li	$t5, 0

search_loop_start:
	lb	$t0, 0($a1)

	beq	$t0, $a2, search_loop_end

	addi	$a1, $a1, 1	

	addi	$t5, $t5, 1
	beq	$t5, $a0, search_loop_fail
	
	j	search_loop_start

search_loop_end:	
	li	$v1, 0
	move	$v0, $t5
	
	jr $ra
search_loop_fail:	
	li	$v1, 1
	
	jr $ra

# Name:		encrypt
#
# Description	Encrypts a message using the vigenere Cipher
#
# Arguments:	$a0 is the alphabet length
# 		$a1 is the address to the alphabet
# 		$a2 is the address to the key
# 		$a3 is the address to the message
# 
# Returns:	None
#
# Destroys:	t0, t1, t2

encrypt:
	addi	$sp, -32
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s2, 12($sp)
	sw	$s3, 16($sp)
	sw	$s4, 20($sp)
	sw	$s5, 24($sp)
	sw	$s6, 28($sp)
		
	move	$s0, $a1		# s0 = start of alphabet
	move	$s2, $a2		# s2 = start of key
	move	$s3, $a3		# s3 = start of message


					# Replace the key with the corresponding
					# numbers from the alphabet
	move	$s4, $s2
encrypt_key_loop:
	lb	$a2, 0($s4)
	beq	$a2, $zero, encrypt_key_loop_end
	
	move	$a1, $s0
	jal	search_alphabet
	
	bne	$v1, $zero, encrypt_key_loop_skip
	
	sb	$v0, 0($s4)
		
encrypt_key_loop_skip:
	addi	$s4, 1
	j encrypt_key_loop

encrypt_key_loop_end:

	addi	$s4, -1			# now, s4 = end of key

					# Replace chars from message with
					# corresponding numbers from alphabet
					# Then shift using key values
	move	$s6, $s2
	move	$s5, $s3
encrypt_message_loop:			
	bne	$s6, $s4, encrypt_message_loop_skip1
	move	$s6, $s2
	
encrypt_message_loop_skip1:
	lb	$a2, 0($s5)
	beq	$a2, $zero, encrypt_message_loop_end
	
	move	$a1, $s0
	jal	search_alphabet
	
	bne	$v1, $zero, encrypt_message_loop_skip2
	
	sb	$v0, 0($s5)
		
	lb	$t0, 0($s6)
	lb	$t1, 0($s5)
	add	$t0, $t0, $t1
	rem	$t0, $t0, $a0
	
	add	$t0, $s0, $t0
	lb	$t2, 0($t0)
	sb	$t2, 0($s5)
	
encrypt_message_loop_skip2:
	addi	$s6, 1
	addi	$s5, 1
	j	encrypt_message_loop
	
encrypt_message_loop_end:
	

	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	lw	$s3, 16($sp)
	lw	$s4, 20($sp)
	lw	$s5, 24($sp)
	lw	$s6, 28($sp)
	addi	$sp, 32
	jr	$ra



# Name:		decrypt
#
# Description	Decrypts a message encrypted with the vigenere Cipher
#
# Arguments:	$a0 is the alphabet length
# 		$a1 is the address to the alphabet
# 		$a2 is the address to the key
# 		$a3 is the address to the message
# 
# Returns:	None
#
# Destroys:	t0, t1, t2

decrypt:
	addi	$sp, -32
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s2, 12($sp)
	sw	$s3, 16($sp)
	sw	$s4, 20($sp)
	sw	$s5, 24($sp)
	sw	$s6, 28($sp)
		
	move	$s0, $a1		# s0 = start of alphabet
	move	$s2, $a2		# s2 = start of key
	move	$s3, $a3		# s3 = start of message


					# Replace the key with the corresponding
					# numbers from the alphabet
	move	$s4, $s2
decrypt_key_loop:
	lb	$a2, 0($s4)
	beq	$a2, $zero, decrypt_key_loop_end
	
	move	$a1, $s0
	jal	search_alphabet
	
	bne	$v1, $zero, decrypt_key_loop_skip
	
	sb	$v0, 0($s4)
		
decrypt_key_loop_skip:
	addi	$s4, 1
	j decrypt_key_loop

decrypt_key_loop_end:

	addi	$s4, -1			# now, s4 = end of key

					# Replace chars from message with
					# corresponding numbers from alphabet
					# Then shift using key values
	move	$s6, $s2
	move	$s5, $s3
decrypt_message_loop:			
	bne	$s6, $s4, decrypt_message_loop_skip1
	move	$s6, $s2
	
decrypt_message_loop_skip1:
	lb	$a2, 0($s5)
	beq	$a2, $zero, decrypt_message_loop_end
	
	move	$a1, $s0
	jal	search_alphabet
	
	bne	$v1, $zero, decrypt_message_loop_skip2
	
	sb	$v0, 0($s5)
		
	lb	$t0, 0($s6)
	lb	$t1, 0($s5)
	sub	$t0, $t1, $t0
	add	$t0, $t0, $a0		# Prevent underflow
	rem	$t0, $t0, $a0
	
	add	$t0, $s0, $t0
	lb	$t2, 0($t0)
	sb	$t2, 0($s5)
	
decrypt_message_loop_skip2:
	addi	$s6, 1
	addi	$s5, 1
	j	decrypt_message_loop
	
decrypt_message_loop_end:
	

	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	lw	$s3, 16($sp)
	lw	$s4, 20($sp)
	lw	$s5, 24($sp)
	lw	$s6, 28($sp)
	addi	$sp, 32
	jr	$ra

jr	$ra
