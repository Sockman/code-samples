/*
  This was written for the class Analysis of Algorithms.
  
  It will find the longest increasing subsequence of a series of integers.
  
  It uses a dynamic programming approach for optimization, and runs in O(n^2) time.
*/

import java.util.Scanner;

public class LongestIncreasingSubseqDP {

    public static void main(String[] args) {
        int[] input = readInput();

        int result = incrSubseq(input.length, input);

        System.out.println(result);
    }


    /**
     * Reads input into a useable format
     */
    private static int[] readInput(){
        Scanner scanner = new Scanner(System.in);
        
        int[] numbers = new int[scanner.nextInt()];

        for (int i = 0; i < numbers.length; i++){
            numbers[i] = scanner.nextInt();
        }

        scanner.close();
        return numbers;
    }

    private static int incrSubseq(int endIndex, int[] input) {
        int biggestLength = 1;

        int[] lengths = new int[endIndex];
        for (int j = 0; j < lengths.length; j++) {
            lengths[j] = 1;
        }

        for (int i = 1; i < endIndex; i++) {

            int localMaxLength = 0;
            for (int j = 0; j < i; j++) {
                if (input[j] < input[i] && lengths[j] > localMaxLength) {
                    localMaxLength = lengths[j];
                }
            }
            lengths[i] = localMaxLength + 1;
            biggestLength = Math.max(biggestLength, lengths[i]);
        }

        return biggestLength;
    }
}
